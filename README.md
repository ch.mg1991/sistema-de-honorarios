# SISTEMA DE HONORÁRIOS

Sistema de emissão de honorários contábeis em PDF com controle de clientes e notas personalizáveis.

## Name
SISTEMA EMISSÃO DE HONORÁRIOS

## Description
Sistema de emissão de honorários contábeis com cadastro de operador, empresa e clientes. Os honorários são gerados em formato .PDF podendo ser personalizados de forma geral ou individual. O sistema ainda possui o histórico de todos os cliente cadastrados e backup do banco de dados feito mensalmente de forma automática. (Obs: Sistema é desenvolvido em JAVA) 

## Installation
Para utilizar o sistema basta executar o arquivo "Recibos.jar" que está localizado dentro do diretório "dist". (Obs: Para executar arquivos JAR é necessário ter a JVM instalada no computador. A mesma econtra-se disponível de forma gratuita no site da [oracle](https://www.java.com/pt-BR/download/manual.jsp))

## Authors and acknowledgment
CARLOS HENRIQUE VENÂNCIO FILHO

## License
Software disponível para uso local e de código aberto. Proibído a venda.

## Project status
Projeto está encerrado. (Finalizado)
